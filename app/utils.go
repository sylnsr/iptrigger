package app

import (
	"net/http"
	"io/ioutil"
	"errors"
	"fmt"
	"crypto/tls"
	"net/url"
	"time"
	"log"
	"os"
	"os/exec"
)

func CheckIp() (string, error) {

	change := "IP: " + EnvCurrentIpAddress
	EnvLastCheck = time.Now().Local().Format(time.RFC3339)
	pushover_message := ""

	// scrape
	data, err := httpGet(EnvScrapeUrl)
	if err != nil { return "", err }

	// parse
	ip, err := parseIpAddress(data, EnvStartText, EnvEndText)
	if err != nil { return "", err }

	// evaluate if the IP has changed
	if ip != EnvCurrentIpAddress {
		// store
		EnvPreviousIpAddress = EnvCurrentIpAddress
		EnvCurrentIpAddress = ip
		// report
		if EnvPreviousIpAddress == "" {
			change = "Set: " + EnvCurrentIpAddress
			pushover_message = EnvHostName + " IP set to " + EnvCurrentIpAddress
		} else {
			change = "Change: " + EnvCurrentIpAddress
			pushover_message = EnvHostName + " IP changed to " + EnvCurrentIpAddress
		}
		// trigger a notification
		notifyVia_PushOver(EnvPushover_token, EnvPushover_user, pushover_message)
		//trigger
		if EnvTriggerFile != "" {
			if _, err := os.Stat(EnvTriggerFile); err == nil {
				if out, err := exec.Command(EnvTriggerFile).Output(); err != nil {
					log.Printf("[ERROR] Failed to exec %s. Error was %s", EnvTriggerFile, err.Error())
				} else {
					log.Printf("Exec %s = %s", EnvTriggerFile, string(out[:]))
				}
			}
		}
	}
	return change, nil
}

func notifyVia_PushOver(token string, user string, message string) {
	s, r, e := httpPost("https://api.pushover.net/1/messages.json",
		map[string]string{
			"token":token,
			"user":user,
			"message":message,
		},
	)
	if e != nil {
		log.Printf("Pushover notification failed: %s\n", e.Error())
	} else {
		log.Printf("Pushover notification result: %d, %s\n", s, r)
	}
}

func httpGet(url string) (body []byte, e error) {

	t := &http.Transport{// skip verify of sever cert
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true },
	}
	client := http.Client{Transport: t }
	resp, e := client.Get(url)

	if e != nil {
		return body, e
	}

	defer resp.Body.Close()

	respBodyBytes, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		return body, errors.New(fmt.Sprintf("Error reading body in HttpGet(..): %s", err2))
	}

	body = respBodyBytes[:]
	return
}

func httpPost(targetUrl string, fields map[string]string) (status int, response string, err error) {

	t := &http.Transport{// skip verify of sever cert
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true },
	}
	client := http.Client{Transport: t }
	urlValues := url.Values{}
	for k := range fields {
		urlValues.Add(k, fields[k])
	}
	resp, e := client.PostForm(targetUrl, urlValues)

	if e != nil {
		err = e
		return
	}

	defer resp.Body.Close()

	respBodyBytes, err2 := ioutil.ReadAll(resp.Body)
	if err2 != nil {
		err = errors.New(fmt.Sprintf("Error reading body in HttpGet(..): %s", err2))
		return
	}

	response = string(respBodyBytes[:])
	status = resp.StatusCode
	return
}


func emitError(w http.ResponseWriter, message string) {
	w.WriteHeader(500)
	w.Write([]byte(message))
}
