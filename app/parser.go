package app

import (
	"strings"
	"unicode/utf8"
)

func parseIpAddress(data []byte, startText string, endText string) (string, error) {
	result := ``

	// if there is a start tag then initialize the result with everything after that point
	if utf8.RuneCountInString(startText) > 0 {
		idxStart := strings.Index(string(data[:]), startText) + utf8.RuneCountInString(startText)
		result = string(data[idxStart:len(data)])
	} else { // else initialize with all the data
		result = string(data[:])
	}

	// if there is an end tag then finish the result by using that
	if utf8.RuneCountInString(endText) > 0 {
		idxEnd := strings.Index(result, endText)
		result = result[0:idxEnd]
	}

	// if no start or end tag were provided then the user will get all the data back as the parsed result

	return result, nil
}
