package app

import (
	"net/http"
	"fmt"
)

func Scan (w http.ResponseWriter, r *http.Request) {
	change, err := CheckIp()
	if (err != nil) {
		emitError(w, err.Error())
		return
	}

	message := EnvScrapeUrl + ` reports ` + change
	if r.URL.Query().Get("iponly") != `` {
		message = change[3:]
	}

	w.WriteHeader(200)
	w.Write([]byte(message))
}

func About (w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	w.Write([]byte(fmt.Sprintf("Service: %s started at %s. Last IP check was at %s",
		EnvInstanceDetail(), EnvStartUp, EnvLastCheck)))
}
