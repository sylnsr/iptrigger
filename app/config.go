package app

import (
	"time"
	"os"
	"fmt"
	"io"
	"crypto/md5"
	"github.com/kardianos/osext"
	"log"
	"github.com/joho/godotenv"
	"path/filepath"
	"strings"
	"strconv"
)

var EnvStartUp, EnvHostName, EnvBuildId, EnvVersion, EnvLastCheck, EnvTriggerFile, EnvCWD string
var EnvCurrentIpAddress, EnvPreviousIpAddress, EnvScrapeUrl, EnvStartText, EnvEndText string
var EnvAutoCheck, EnvServicePort int
var EnvPushover_token, EnvPushover_user string

func init() {
	EnvVersion = "0.0.3"
	EnvStartUp = time.Now().Local().Format(time.RFC3339)
	EnvHostName = hostName()
	EnvBuildId = buildId()
	EnvLastCheck = "<never>"

	// show app info at startup
	log.Printf("%s\n", EnvInstanceDetail())
	log.Printf("Source: https://github.com/sylnsr/iptrigger\n")

	// config file loading
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}
	EnvCWD = dir
	EnvConfigFile := envOrDefault("IPTRIGGER_config", EnvCWD + "/iptrigger.conf")
	log.Println("Looking for config in " + EnvConfigFile)
	if e := godotenv.Load(EnvConfigFile); e != nil {
		log.Println("Config file not found. I'll try loading from environment variables.")
	}

	// load parameters from config
	setConfig()

	// just in case no config params where provided at all:
	if EnvPushover_token == "< your Pushover use-key >" || EnvPushover_user == "< your Pushover use-key >" {
		log.Println("The app requires a Pushover token and key")
		os.Exit(-2)
	}

}

func setConfig () {
	// required params
	EnvPushover_user = envOrDefault("IPTRIGGER_pushover_user", "< your Pushover use-key >")
	EnvPushover_token = envOrDefault("IPTRIGGER_pushover_token", "< your Pushover token >")
	// optional params ~ should work without change but not recommended for anything other than demonstration purposes
	EnvServicePort = envOrDefaultInt("IPTRIGGER_port", 3030)
	EnvScrapeUrl = envOrDefault("IPTRIGGER_scrapeUrl", "https://diagnostic.opendns.com/myip")
	EnvStartText = envOrDefault("IPTRIGGER_startText", "")
	EnvEndText = envOrDefault("IPTRIGGER_endText", "")
	EnvAutoCheck = envOrDefaultInt("IPTRIGGER_auto_check", 5)
	EnvTriggerFile = envOrDefault("IPTRIGGER_trigger_script", "/iptrigger.sh")
}

func envOrDefault(key string, defaultVal string) string {

	if osVal := strings.Trim(os.Getenv(key), " "); osVal != "" {
		log.Printf("Using your config param %s = %s\n", key, osVal)
		return osVal
	} else {
		log.Printf("Using default demo param %s = %s\n", key, defaultVal)
		return defaultVal
	}
}

func envOrDefaultInt(key string, defaultVal int) int {

	if osVal := strings.Trim(os.Getenv(key), " "); osVal != "" {
		if i, e := strconv.Atoi(osVal); e != nil {
			log.Printf("[FATAL] Environment var %s=%s? ... %s needs to be an integer please. Now, I die!  x_x\n", key, osVal, osVal)
		} else {
			return i
		}
	}
	return defaultVal
}

func hostName() string {
	hostNameStr, err := os.Hostname()
	if err != nil {
		return "<unknown>"
	} else {
		return hostNameStr
	}
}

func buildId() string {

	getHash := func() ([]byte, error) {
		var result []byte

		if exePath, err := osext.Executable(); err != nil {
			return result, err

		} else {
			file, err := os.Open(exePath)
			if err != nil {
				return result, err
			}
			defer file.Close()

			hash := md5.New()
			if _, err := io.Copy(hash, file); err != nil {
				return result, err
			}
			return hash.Sum(result), nil
		}
	}

	v, e := getHash()
	if e != nil {
		fmt.Printf("[FATAL] %v", e)
		panic(-1)
	}
	return fmt.Sprintf("%x", v)
}

// Returns a string describing the running instance
func EnvInstanceDetail() string {
	return fmt.Sprintf("iptrigger-%s; build:%s; host:%s",
		EnvVersion, EnvBuildId, EnvHostName)
}
