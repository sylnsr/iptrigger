#!/usr/bin/env bash
echo "Building your own image, named 'iptrigger'"
docker build -t iptrigger .
echo "Done:"
docker images | grep iptrigger