# iptrigger
iptrigger scrapes a specified website to detect if your WAN IP address has changed and if it has, triggers a notification via Pushover and executes a script of your choice.

### Why
I created iptrigger so that I can run certain web services (of minor importance) on my home network without paying for a static IP. When my ISP changes my IP address, I run a script to update my DNS records, amongst other things.

### When
The app can be configured to run in intervals by the minutes you specify. Setting the interval to 0 turns this feature off. The app also launches a listener on a port your specify so that you can make a GET request to trigger a check. This is useful in-case you want to run an iptrigger check via an external app like cron.

### Where
Run the app from any directory you want - just make sure that you also have the `iptrigger.conf` file in the same directory. Of course a symlink can also be used in case you want the actual config file in a different location.

### Who
You can connect to any website you specify and configure to be parsed. It is up to you to make sure you are not in violation of their terms. I accept no responsibility for which site you configure to be scraped. I don't even make any recommendations, so don't ask me.

### What
- The app was written in go (aka golang, for 1.5) - it has a tiny footprint
- It can also be run as a Docker container - `Dockerfile` provided
- License: [GNU GPL v3](http://choosealicense.com/licenses/gpl-3.0/) 

### How
1. Create a file named `iptrigger.conf` in the same directory where the app runs from
1. Setup the configuration parameters
1. Run the app any way you want to and it will launch the service.


## Configuration
With the exception of the `IPTRIGGER_config` parameter, all configuration parameters can be loaded from environment variables or from a configuration file.  
**NOTE:**  If config parameters exists as environment variables and config file variables, then the config file will take precedence.  


### How to load a config file
You can either set an environment variable named `IPTRIGGER_config` with the path to your config file .. **_or_** .. you can create a file named `iptrigger.conf` and place it in the same folder as the folder where you are running the app from.  
If you have both then the environment variable will take precedence.

### File example  

    # optional, the default is 3030
    IPTRIGGER_port=3030
    
    # URL of the site to scrape
    IPTRIGGER_scrapeUrl=http://myownwebsite.com/ipaddress.php
    
    # The source-code text that appears immediately BEFORE the IP address from the site scraped
    IPTRIGGER_startText=<REMOTE_ADDR>
    
    # The source-code text that appears immediately AFTER the IP address from the site scraped
    IPTRIGGER_endText=</REMOTE_ADDR>
    
    # Your Pushover.net API token
    IPTRIGGER_pushover_token=<foo>
    
    # Your Pushover.net user key
    IPTRIGGER_pushover_user=<bar>
    
    # Auto-check the IP every <IPTRIGGER_auto_check> minutes
    IPTRIGGER_auto_check=5
    
    # Path to a script to execute when the IP address has changed
    # Nothing will happen if the file cannot be found
    # The file needs to be executable and should contain a she-bang at the top
    IPTRIGGER_trigger_script=./iptrigger.sh  
  
## Troubleshooting
Bug: `[ERROR] Failed to exec ./iptrigger.sh. Error was fork/exec ./iptrigger.sh: permission denied`  
Fix: Make sure the script you are trying to execute has read **and** execute permissions. Try `chmod +x ./iptrigger.sh`  
  
## Docker
**Firstly, I don't recommend running this app in Docker if you don't need to**. The app is not that complicated and only takes up about 7MB. Building the Docker image, using the official image for GO, will take about 700MB.  
That being said, perhaps you are a Windows user and want to use this app or your philosophy is to 'docker all the things!'. Build the image in your Linux VM and run it in Docker on Windows.  
Before building the image though, make sure that you edit the `iptrigger.sh` file to do what you want it to do and build yourself a new `iptrigger.conf` file from the provided `iptrigger.conf.dist` file.  

## Future plans
- [x] Provide systemd sample script  
- [ ] Implement OpenDNS client API

## Support
No support or warranty for fitness of use is provided ... although I might take suggestions to make improvements.  
  
