package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"github.com/sylnsr/iptrigger/app"
	"time"
	"fmt"
)

func main() {

	// SCHEDULED
	// if app.EnvAutoCheck is set then check the IP it every <app.EnvAutoCheck> minutes
	if app.EnvAutoCheck > 0 {
		go func() {
			for {
				app.CheckIp()
				time.Sleep(time.Duration(app.EnvAutoCheck) * time.Minute)
			}
		}()
	}

	// ON-DEMAND
	// create a service that can be hit (GET) to check the IP
	r := mux.NewRouter()
	createMuxRoutes(r);
	port := fmt.Sprintf(":%d", app.EnvServicePort)
	log.Printf("Starting IP Trigger on http://localhost" + port)
	log.Printf("---------------------------")
	log.Printf("Press Ctl-C to stop the app")
	log.Printf("---------------------------")
	http.ListenAndServe(port, r)

}

func createMuxRoutes(r *mux.Router) {
	// Put something at "/"
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte("It works!"))
	}).Methods("GET")

	// Tell me to scan for an IP change
	r.HandleFunc("/scan", app.Scan).Methods("GET")

	// Tell me about yourself
	r.HandleFunc("/about", app.About).Methods("GET")
}