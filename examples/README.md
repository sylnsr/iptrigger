# Examples

This folder contains examples files for OSX's launchd, and on Linux .. systemd.

## OSX
The launchd .plist file is `com.github.sylnsr.iptrigger.plist`. A recommended place to put the file is in `~/Library/LaunchAgents/`. The only change you need to make to this file is to set the paths to whatever they are according to your iptrigger setup. Once you have the file in place, load it into launchctl with:  
`launchctl load com.github.sylnsr.iptrigger.plist`
 
## Systemd
The systemd file is `iptrigger.service` and to make it a system job, place it in `/etc/systemd/system/`
