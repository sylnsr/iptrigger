FROM golang:1.5

RUN mkdir -p /go/src/github.com/sylnsr/iptrigger
WORKDIR /go/src/github.com/sylnsr/iptrigger
COPY . /go/src/github.com/sylnsr/iptrigger
RUN go-wrapper download
RUN go-wrapper install

# This port only works if you are using the default config value
EXPOSE 3030 3030

CMD ["/go/bin/iptrigger"]