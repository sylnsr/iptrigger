#!/usr/bin/env bash

ts=$(date +%F_%T)
echo "$ts IP changed/set" >> ./iptrigger.log
echo "Script executed"